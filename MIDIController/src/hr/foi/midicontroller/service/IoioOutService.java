package hr.foi.midicontroller.service;

import hr.foi.midicontroller.bridge.IoioBridge;
import hr.foi.midicontroller.core.MidiMessage;
import ioio.lib.api.DigitalOutput.Spec;
import ioio.lib.api.DigitalOutput.Spec.Mode;
import ioio.lib.api.Uart;
import ioio.lib.api.Uart.Parity;
import ioio.lib.api.Uart.StopBits;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOService;

import java.io.IOException;
import java.io.OutputStream;

import android.content.Intent;
import android.os.IBinder;
import android.util.Log;


public class IoioOutService   extends IOIOService implements IService {

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	protected IOIOLooper createIOIOLooper() {		
		return new BaseIOIOLooper() {		 
			private OutputStream _out; 
			private Uart _midi_out;

			
			/***
			 * On every successful connect with IOIO board this method is executed.
			 */
			@Override
			protected void setup() throws ConnectionLostException,
					InterruptedException {

				//open pin 7 with baud rate 31250
				_midi_out = ioio_.openUart(null,new Spec(7,Mode.OPEN_DRAIN), 31250,Parity.NONE,StopBits.ONE);
				//open output stream
				_out = _midi_out.getOutputStream();		
				sendBroadcastMessage("midi.board.CONNECTED");				
			}
			
			@Override
			public void disconnected() {
				//sending broadcast
			    sendBroadcastMessage("midi.board.DISCONNECTED");
			    super.disconnected();	
			}
			/***
			 * This loop is active while connection is alive with IOIO board.
			 */
			@Override
			public void loop() throws ConnectionLostException,
					InterruptedException {
				
				MidiMessage m = IoioBridge._outputBuffer.poll();
				while( m!=null ){
					try {								
						_out.write(m.getHexValue());
						
						m =  IoioBridge._outputBuffer.poll();
					} catch (IOException e) {
						e.printStackTrace();
					}	
				}				
			}
		};
	}

	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);	
	}

	@Override
	public void sendBroadcastMessage(String intentAction) {
		//sending broadcast
		Intent in= new Intent();
	    in.setAction(intentAction);
	    Log.d("BROADCAST", "SERVICE IOIO:" + intentAction);
	    sendBroadcast(in);		
	}

}
