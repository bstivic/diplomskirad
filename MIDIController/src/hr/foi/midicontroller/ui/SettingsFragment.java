package hr.foi.midicontroller.ui;

import hr.foi.midicontroller.R;
import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView.OnItemSelectedListener;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

public class SettingsFragment extends Fragment implements OnItemSelectedListener{
	public static final String TAG = "SettingsFragment";
	OnButtonPressedListener mCallback;
	private MainActivity activity;
	//Buttons
	Button btnBack;
	Spinner channel;
	SeekBar seekModThres;
	SeekBar seekModSens;
	SeekBar seekPitchThres;
	SeekBar seekPitchSens;
	
	
	public interface OnButtonPressedListener {
		public void onBackPressed();
		public void onPitchSensitivityChanged(int value);

	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View inflaterView  = inflater.inflate(R.layout.settings_fragment, container, false);		
		//btnBack = (Button) inflaterView.findViewById(R.id.btnBack);		
		//btnBack.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {backToKeys();}});
		channel=(Spinner)  inflaterView.findViewById(R.id.spinnerChannel);	
		seekModThres=(SeekBar)  inflaterView.findViewById(R.id.seekMThresh);	
		seekModSens=(SeekBar)  inflaterView.findViewById(R.id.seekModSens);	
		seekPitchThres=(SeekBar)  inflaterView.findViewById(R.id.seedPitchThre);	
		seekPitchSens=(SeekBar)  inflaterView.findViewById(R.id.seekPitchSens);	
		
		seekModThres.setMax(100);
		seekModSens.setMax(100);
		seekPitchThres.setMax(100);
		seekPitchSens.setMax(100);

		
		seekPitchSens.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar arg0, int value, boolean arg2) {
				mCallback.onPitchSensitivityChanged(value);
				
			}
		});
		
		
		
		
		
		final Integer[] items = new Integer[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
		
		ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(activity,android.R.layout.simple_spinner_item, items);
		channel.setAdapter(adapter);
		
		return inflaterView;
	}
	@Override
	public void onResume() {
	    //TextView spinnerText = (TextView) channel.getChildAt(0);
		//		spinnerText.setTextColor(Color.WHITE);
		channel.setSelection(activity.app.getChannel()-1);
		super.onResume();
	}

	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity)activity;

        try {
            mCallback = (OnButtonPressedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnButtonPressedListener");
        }
    }


	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		 ((TextView) arg0.getChildAt(0)).setTextColor(Color.WHITE);
	       activity.app.setChannel(arg2+1);
	   
		
	}


	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

	


	
	//**************************************************************************************************
	//PRIVATE METHODS
	//**************************************************************************************************

}
