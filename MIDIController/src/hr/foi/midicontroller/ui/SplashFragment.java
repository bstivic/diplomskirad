package hr.foi.midicontroller.ui;

import hr.foi.midicontroller.R;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class SplashFragment extends Fragment {
	public static final String TAG = "SplashFragment"; 
	OnButtonPressedListener mCallback;
	
	//buttons
	Button btnKeyboard;
	Button btnSettings;
	Button btnExit;
	
	//================================================================================
    // Interface
    //================================================================================	
	public interface OnButtonPressedListener {
		public void onKeyboardPressed();
		public void onSettingsPressed();
		public void onExitPressed();
	}
	
	//================================================================================
    // Life-cycle
    //================================================================================	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View inflaterView  = inflater.inflate(R.layout.splash_fragment, container, false);	
		
		btnKeyboard = (Button) inflaterView.findViewById(R.id.btnKeyboard);		
		btnSettings = (Button)inflaterView.findViewById(R.id.btnSettings);
		btnExit = (Button) inflaterView.findViewById(R.id.btnExit);
		
		btnKeyboard.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {openKeyboard();}});
		btnSettings.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {openSettings();}});
		btnExit.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {exitApplication();}});
		
		
		return inflaterView;
	}
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        try {
            mCallback = (OnButtonPressedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnButtonPressedListener");
        }
    }
	
	
	//================================================================================
    // Methods
    //================================================================================
	public void openKeyboard() {
		mCallback.onKeyboardPressed();
	}
	public void openSettings(){
		mCallback.onSettingsPressed();
	}
	public void exitApplication(){
		mCallback.onExitPressed();
	}
}
